//select Element
let countSpan = document.querySelector(".quiz-info .count span");

let bullets = document.querySelector(".bullets");

let bulletsSpanContainer = document.querySelector(".bullets .spans");

let quizArea = document.querySelector(".quiz-area");

let answersArea = document.querySelector(".answers-area");

let submitBtn = document.querySelector(".submit-btn");
let resultConatiner = document.querySelector(".results");

let countDownSpan = document.querySelector('.countdown');

//Set Option 
let currentIndex = 0;
let rightAnswers = 0;
let countDownInterval;

let QuestionTimer = 120;
function  getQuestions() {

    let myRequest = new XMLHttpRequest();
    myRequest.onreadystatechange = function(){
        if(this.readyState === 4 && this.status === 200){

            let questionObject = JSON.parse(this.responseText);
            let qCount = questionObject.length;
            //console.log(questionObject);

            //create Bulletas + set Question Count
            createBullets(qCount);

            //Add Question data
            addQuestionData(questionObject[currentIndex],qCount);


            //Start Count Down
            countdown(QuestionTimer,qCount);

            //click on submit 
            submitBtn.onclick = function(){
                //get Right ANswer
                let theRightAnswer = questionObject[currentIndex].right_answer;

                //Incease Index
                currentIndex++;

                //check the Answer 
                checkAnswer(theRightAnswer,qCount);

                //Remove Previous Question
                quizArea.innerHTML = "";
                answersArea.innerHTML = "";

                //Add Question data
                addQuestionData(questionObject[currentIndex],qCount);

                //Handel Bullets Class
                handleBullets();

                //Start Count Down
                clearInterval(countDownInterval);
                countdown(QuestionTimer,qCount);

                //Show Result 
                showResults(qCount);
            }
        }
    }
    myRequest.open("GET","json/questions.json",true);
    myRequest.send();
}
getQuestions();

function createBullets(num){
    countSpan.innerHTML = num;

    //create spans
    for(let i = 0;i < num ; i++){

        //create Span
        let theBullet = document.createElement("span");

        //check if   its first span
        if(i === 0){
            theBullet.className = "on";
        }

        //Append bullets to main bullets container
        bulletsSpanContainer.appendChild(theBullet);

    }
}


function addQuestionData(obj,count){
    // console.log(obj);
    // console.log(count);
    if(currentIndex < count){
        //create question title
        let questionTitle = document.createElement("h2");

        //create question text 
        let questionText = document.createTextNode(obj['title']);

        //Apppend  tet to h2
        questionTitle.appendChild(questionText);

        //Append  the h2  to tthe quizArea
        quizArea.appendChild(questionTitle);


        //create the answer 
        for(let i = 1;i <= 4; i++){
            //create Main Answer Div
            let MainDiv = document.createElement("div");


            //Add class To Mian Div 
            MainDiv.className = "answer";

            //create radio Input
            let radioInput = document.createElement("input");


            //Add Type And Id And data Attribuite
            radioInput.name = "question";
            radioInput.type = "radio";
            radioInput.id = `answer_${i}`;
            radioInput.dataset.answer = obj[`answer_${i}`];


            //Make First Option Selected
            if(i === 1){
                radioInput.checked = true;
            }

            //create Label
            let theLabel = document.createElement("label");

            //Add For Attribute
            theLabel.htmlFor = `answer_${i}`;

            //create table text
            let theLabelText = document.createTextNode(obj[`answer_${i}`]);

            //Add the Text To Label 
            theLabel.appendChild(theLabelText);

            //Add Input To Main Dive
            MainDiv.appendChild(radioInput);
            MainDiv.appendChild(theLabel);

            //Append All Divs To answersArea
            answersArea.appendChild(MainDiv);
        }
    
    }    
}

function checkAnswer(right_answer,count){
    // console.log(right_answer);
    // console.log(count);
    let answers = document.getElementsByName("question");
    let theChoosenAnswer;

    for(let i = 0;i < answers.length;i++){
        if(answers[i].checked){
            theChoosenAnswer = answers[i].dataset.answer;
        }
    }
    console.log(right_answer);
    console.log(theChoosenAnswer);
    if(right_answer === theChoosenAnswer){
        rightAnswers++; 
        console.log("good Answer");
    }

}

function handleBullets(){
    let bulletsSpan = document.querySelectorAll(".bullets .spans span");
    let arrayOfSpan = Array.from(bulletsSpan);
    arrayOfSpan.forEach((span,index)=>{
        if(currentIndex === index){
            span.className = "on";
        }
    })
}

function showResults(count){
    let theResult;
    if(currentIndex === count){
        //console.log("finish");
        quizArea.remove();
        answersArea.remove();
        submitBtn.remove();
        bullets.remove();

        if(rightAnswers > (count / 2)&& rightAnswers < count){
            theResult = `<span class="good">Good</span>,${rightAnswers} From ${count} Is Good Level`;

        }else if(rightAnswers ===count){
            theResult = `<span class="perfect">Perfect</span>,All Answers is Good`;
        }else{
            theResult = `<span class="bad">shite man!</span>, All Answers is Bad`;
        }
        resultConatiner.innerHTML = theResult;
        resultConatiner.style.marginTop = "15px";
        resultConatiner.style.backgroundColor = "#eeeeee";
    }
}

function countdown(duration,count){
    if(currentIndex < count){
        //
        let minutes,seconds;
        countDownInterval = setInterval(function(){

            minutes = parseInt(duration / 60);
            seconds = parseInt(duration % 60);

            minutes = minutes < 10 ? `0${minutes}`: minutes;
            seconds = seconds < 10 ? `0${seconds}`: seconds;
            countDownSpan.innerHTML = `${minutes} : ${seconds}`;

            if(--duration < 0){
                clearInterval(countDownInterval);
                //console.log("Time Is Losed");
                submitBtn.click();
            }
        },1000);
    }
}